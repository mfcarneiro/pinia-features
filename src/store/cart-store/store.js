import { defineStore, acceptHMRUpdate } from "pinia";
import { groupBy } from "lodash";

import { useAuthStore } from "../auth-store/store.js";
import { useLocalStorage } from "@vueuse/core";

export const cartStore = defineStore("CartStore", {
  state: () => {
    return {
      items: useLocalStorage("CartStore:items", []),
    };
  },
  getters: {
    // Old way
    // cartCount() {
    //   return this.items.length;
    // },
    cartCount: (state) => state.items.length,
    isCartEmpty: (state) => state.cartCount === 0,
    groupedCartItems: (state) => {
      const groupItems = groupBy(state.items, (item) => item.name);
      const sorted = Object.keys(groupItems).sort();
      let inOrder = {};

      sorted.forEach((key) => (inOrder[key] = groupItems[key]));

      return inOrder;
    },
    groupedCartCount: (state) => (name) => state.groupedCartItems[name].length,
    cartTotalPrice: (state) =>
      state.items.reduce((acc, current) => acc + current.price, 0),
  },
  actions: {
    addItems(count, item) {
      count = parseInt(count);
      for (let index = 0; index < count; index++) {
        this.items.push({ ...item });
      }
    },
    deleteCartItem(cartItemName) {
      this.items = this.items.filter(({ name }) => name !== cartItemName);
    },
    updateCartItemCount(item, count) {
      this.deleteCartItem(item.name);
      this.addItems(count, item);
    },
    checkout() {
      const authStore = useAuthStore();

      alert(
        `${authStore.username} just have bought ${this.cartCount} item(s at a total of $${this.cartTotalPrice}`
      );
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(cartStore, import.meta.hot));
}
