import { productStore } from "./product-store/store.js";
import { cartStore } from "./cart-store/store.js";

export const useProductStore = productStore;
export const useCartStore = cartStore;
