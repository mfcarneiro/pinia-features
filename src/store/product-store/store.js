import { defineStore } from "pinia";

export const productStore = defineStore("ProductStore", {
  state: () => {
    return {
      products: [],
    };
  },
  actions: {
    async loadStandProducts() {
      this.products = (await import("@/data/products.json")).default;
    },
  },
});
